import React from 'react';
import { Provider } from 'react-redux';
import { store } from './src/store/store';
import Root from "./src/Root";
import {Provider as PaperProvider} from 'react-native-paper'
export default function App() {
  return (
    <Provider store={store}>
      <PaperProvider>
      <Root />
      </PaperProvider>
    </Provider>
  );
}