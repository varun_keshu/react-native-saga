import React from 'react';
import {Text} from 'react-native';
import styles from './TextStyle';
const TextField = ({
  title,
  ...props
}) => (
  <Text
    style={styles.textFieldDefault}
    {...props}
> {title}
  </Text>
);

export default TextField;
