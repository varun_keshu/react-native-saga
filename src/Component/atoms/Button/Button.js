import React from 'react';
import { TouchableOpacity, View,Text } from 'react-native';
import styles from './ButtonStyle';
import { Button } from 'react-native-paper';



const Btn = ({ title, onPress, style, textStyle,bottom, ...props }) => (
<View style={styles.submitBtnContainer}>
<Button onPress={onPress} {...props}style={[,bottom?styles.bottomBtn:styles.container,style]} >

		<Text style={[styles.btnText,textStyle]}>{title}</Text>

</Button>
</View>	
);

export default Btn;
