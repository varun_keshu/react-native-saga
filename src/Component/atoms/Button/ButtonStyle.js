import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
	container: {
		backgroundColor:"lightblue",
		borderRadius:5,
		paddingVertical:10,
		marginHorizontal:20
		
	},
	submitBtnContainer: {
		width:'100%',
	},
	bottomBtn:{
		borderRadius:3,
		paddingVertical:10,
		justifyContent:'flex-end',
		alignContent:'space-between',
		position:'absolute',
		bottom:wp(2),
		width:'100%',
	},
	btnText: {
		color: "black",
		fontSize:wp(5),
		textAlign:'center',
		fontWeight:'700',

	}, 
	
	

	
});
export default styles;