
import { combineReducers } from 'redux';
import counterReducer from '../reducers/componentReducer';
const rootReducer = combineReducers({
  counter: counterReducer,
});
export default rootReducer;