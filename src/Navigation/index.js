import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LogInScreen from '../screen/LoginScreen/LogInScreen';
import HomeScreen from '../screen/HomeScreen/HomeScreen';

const Stack = createStackNavigator();

export default function Navigation() {
	return (
		<NavigationContainer>
			<Stack.Navigator>
			<Stack.Screen 
                options={{ headerShown: true }} 
                name="Login" 
                component={LogInScreen} /> 
				
                <Stack.Screen 
                options={{ headerShown: true }} 
                name="Home" 
                component={HomeScreen} /> 

			</Stack.Navigator>
		</NavigationContainer>
	);
}
