import { StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container : {
        alignItems: "center",
        backgroundColor: "#fff",
        paddingTop : 20,
    },    
    txtColor: {
        fontSize:40,
        fontWeight: "bold",
        color: "black", 
    },
    inputView: {
        width: "90%",
        marginBottom: 20,   
     
    },
    TextInput: {
        margin: 10,
        fontSize: 15,
        fontWeight: "bold",
      },
      btnStyle : {
        borderRadius:3,
        width : "100%",

      }
});
export default styles;

